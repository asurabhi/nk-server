
CREATE TABLE catalogs ( 
	catalog_id           int  NOT NULL  AUTO_INCREMENT,
	name                 varchar(256)  NOT NULL,
	CONSTRAINT pk_catalogs PRIMARY KEY ( catalog_id )
 );

CREATE TABLE categories ( 
	category_id          int  NOT NULL  AUTO_INCREMENT,
	catalog_id           int  NOT NULL,
	name                 varchar(256)  NOT NULL,
	CONSTRAINT pk_categories PRIMARY KEY ( category_id ),
	CONSTRAINT categories_ibfk_1 FOREIGN KEY ( catalog_id ) REFERENCES catalogs( catalog_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 );

CREATE INDEX catalog_id ON categories ( catalog_id );

CREATE TABLE category_attributes ( 
	category_attribute_id int  NOT NULL  AUTO_INCREMENT,
	category_id          int  NOT NULL,
	name                 varchar(256)  NOT NULL,
	component_type       enum('textbox','dropdown','checkbox','radiobutton','numberbox','spinner')   NOT NULL,
	data_type            enum('integer','decimal','string','boolean','date')   NOT NULL,
	default_value        varchar(256),
	min_value            int,
	max_value            int,
	CONSTRAINT pk_category_attributes PRIMARY KEY ( category_attribute_id ),
	CONSTRAINT category_attributes_ibfk_1 FOREIGN KEY ( category_id ) REFERENCES categories( category_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 );

CREATE INDEX category_id ON category_attributes ( category_id );

CREATE TABLE company_circles ( 
	company_circle_id    int  NOT NULL  AUTO_INCREMENT,
	name                 varchar(200),
	domain_name          varchar(400),
	created_date         datetime,
	CONSTRAINT pk_company_circle PRIMARY KEY ( company_circle_id )
 ) engine=InnoDB;

ALTER TABLE company_circles COMMENT 'This table will have all the companies circles, this table will be populated over the time as the people from different company signs up for this application. We will use the domain name part of the official mail id(during registration/ during posting an ad) as the key to the circle.';

CREATE TABLE states ( 
	state_id             int  NOT NULL  AUTO_INCREMENT,
	name                 varchar(256)  NOT NULL,
	CONSTRAINT pk_states PRIMARY KEY ( state_id )
 );

CREATE TABLE users ( 
	user_id              int  NOT NULL  AUTO_INCREMENT,
	emailid              varchar(200),
	password             varchar(256),
	official_emailid     varchar(200),
	name                 varchar(256)  NOT NULL,
	contact_number       int  NOT NULL,
	is_system_generated  bit   DEFAULT b'0' ,
	is_third_party_login bit   DEFAULT b'0' ,
	official_emailid_verified bool   DEFAULT false ,
	CONSTRAINT pk_users PRIMARY KEY ( user_id )
 );

ALTER TABLE users MODIFY official_emailid varchar(200)     COMMENT 'when the user registers this official_emailid will be provided, which we can use to place the ad in his company circle and also use to show the ads in his circle. When he switches company he can update this mail id.';

CREATE TABLE category_attribute_options ( 
	category_attribute_option_id int  NOT NULL  AUTO_INCREMENT,
	category_attribute_id int,
	serial_number        int,
	value                varchar(100),
	CONSTRAINT pk_category_attribute_options PRIMARY KEY ( category_attribute_option_id ),
	CONSTRAINT fk_category_attribute_options FOREIGN KEY ( category_attribute_id ) REFERENCES category_attributes( category_attribute_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) engine=InnoDB;

CREATE INDEX idx_category_attribute_options ON category_attribute_options ( category_attribute_id );

ALTER TABLE category_attribute_options COMMENT 'for dropdown,checkbox,radiobutton component_type`s in category_attributes table we will store the relevant options in this table.';

CREATE TABLE city ( 
	city_id              int  NOT NULL  AUTO_INCREMENT,
	name                 varchar(256)  NOT NULL,
	state_id             int  NOT NULL,
	tier                 int,
	CONSTRAINT pk_city PRIMARY KEY ( city_id ),
	CONSTRAINT city_ibfk_1 FOREIGN KEY ( state_id ) REFERENCES states( state_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 );

CREATE INDEX state_id ON city ( state_id );

ALTER TABLE city MODIFY tier int     COMMENT 'whether this city is tier 1 or 2 or 3';

CREATE TABLE locality ( 
	locality_id          int  NOT NULL  AUTO_INCREMENT,
	name                 varchar(256)  NOT NULL,
	city_id              int  NOT NULL,
	landmark             varchar(256),
	pincode              int  NOT NULL,
	loc_lat              int,
	loc_lag              int,
	CONSTRAINT pk_locality PRIMARY KEY ( locality_id ),
	CONSTRAINT locality_ibfk_1 FOREIGN KEY ( city_id ) REFERENCES city( city_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 );

CREATE INDEX city_id ON locality ( city_id );

CREATE TABLE ad_details ( 
	ad_detail_id         int  NOT NULL  AUTO_INCREMENT,
	name                 varchar(500)  NOT NULL,
	price                double,
	description          varchar(5000),
	price_negotiable     bool,
	personal_mailid      varchar(200),
	personal_mailid_visible bool   DEFAULT false ,
	search_tags_csv      varchar(2000),
	created_ts           datetime  NOT NULL,
	last_modified_ts     datetime,
	adressline1          varchar(400),
	user_id              int  NOT NULL,
	city_id              int  NOT NULL,
	locality_id          int,
	category_id          int  NOT NULL,
	status               enum('Draft','Active','Inactive','Delete')    DEFAULT 'Draft' ,
	official_emailid     varchar(200),
	contact_number       bigint,
	other_locality       varchar(200),
	priority             int,
	CONSTRAINT pk_ad_detail_v2 PRIMARY KEY ( ad_detail_id ),
	CONSTRAINT fk_ad_detail_v2_city FOREIGN KEY ( city_id ) REFERENCES city( city_id ) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_ad_detail_v2_locality FOREIGN KEY ( locality_id ) REFERENCES locality( locality_id ) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_ad_detail_categories FOREIGN KEY ( category_id ) REFERENCES categories( category_id ) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_ad_detail_users FOREIGN KEY ( user_id ) REFERENCES users( user_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) engine=InnoDB;

CREATE INDEX idx_ad_detail_v2 ON ad_details ( city_id );

CREATE INDEX idx_ad_detail_v2_0 ON ad_details ( locality_id );

CREATE INDEX idx_ad_detail ON ad_details ( category_id );

CREATE INDEX idx_ad_detail_0 ON ad_details ( user_id );

ALTER TABLE ad_details MODIFY official_emailid varchar(200)     COMMENT 'We will pre-populate this field while posting an ad in case the user already has this field set up during registration (i.e in users table)';

ALTER TABLE ad_details MODIFY contact_number int     COMMENT 'We will pre-populate this field with users.contact_number column while creating the ad with the option for the user to over ride the same.';

ALTER TABLE ad_details MODIFY other_locality varchar(200)     COMMENT 'in case the user wants to enter a different locality than the listed options';

CREATE TABLE ad_images ( 
	ad_image_id          int  NOT NULL  AUTO_INCREMENT,
	ad_detail_id         int  NOT NULL,
	image_file_path      varchar(256)  NOT NULL,
	CONSTRAINT pk_ad_images PRIMARY KEY ( ad_image_id ),
	CONSTRAINT ad_images_ibfk_1 FOREIGN KEY ( ad_detail_id ) REFERENCES ad_details( ad_detail_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX ad_detail_id ON ad_images ( ad_detail_id );

CREATE TABLE category_attribute_instance ( 
	category_attribute_instance_id int  NOT NULL  AUTO_INCREMENT,
	category_attribute_id int  NOT NULL,
	ad_detail_id         int  NOT NULL,
	component_value      varchar(256),
	CONSTRAINT pk_category_attribute_instance PRIMARY KEY ( category_attribute_instance_id ),
	CONSTRAINT category_attribute_instance_ibfk_2 FOREIGN KEY ( category_attribute_id ) REFERENCES category_attributes( category_attribute_id ) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT category_attribute_instance_ibfk_1 FOREIGN KEY ( ad_detail_id ) REFERENCES ad_details( ad_detail_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX ad_detail_id ON category_attribute_instance ( ad_detail_id );

CREATE INDEX category_attribute_id ON category_attribute_instance ( category_attribute_id );

CREATE TABLE category_attribute_option_instances ( 
	category_attribute_option_instance_id int  NOT NULL,
	category_attribute_instance_id int,
	category_attribute_option_id int,
	CONSTRAINT pk_category_attribute_option_instances PRIMARY KEY ( category_attribute_option_instance_id ),
	CONSTRAINT fk_category_attribute_option_instances FOREIGN KEY ( category_attribute_instance_id ) REFERENCES category_attribute_instance( category_attribute_instance_id ) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_category_attribute_option_instances_1 FOREIGN KEY ( category_attribute_option_id ) REFERENCES category_attribute_options( category_attribute_option_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) engine=InnoDB;

CREATE INDEX idx_category_attribute_option_instances ON category_attribute_option_instances ( category_attribute_instance_id );

CREATE INDEX idx_category_attribute_option_instances_0 ON category_attribute_option_instances ( category_attribute_option_id );

CREATE TABLE ad_detail_company_circle ( 
	ad_detail_company_circle_id int  NOT NULL,
	company_circle_id    int,
	ad_detail_id         int,
	verified_ad          bool   DEFAULT false ,
	CONSTRAINT pk_ad_detail_company_circle PRIMARY KEY ( ad_detail_company_circle_id ),
	CONSTRAINT fk_ad_detail_company_circle FOREIGN KEY ( company_circle_id ) REFERENCES company_circles( company_circle_id ) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_ad_detail_company_circle_0 FOREIGN KEY ( ad_detail_id ) REFERENCES ad_details( ad_detail_id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) engine=InnoDB;

CREATE INDEX idx_ad_detail_company_circle ON ad_detail_company_circle ( ad_detail_id );

CREATE INDEX idx_ad_detail_company_circle_0 ON ad_detail_company_circle ( company_circle_id );

ALTER TABLE ad_detail_company_circle COMMENT 'This table will have association between the ad_detail and company_circle. When somebody wants to post an ad in a company circle we will maintain that association here. For eg. a chartered accountant can post an ad to infosys employees. The user can choose to target an ad to a particular circle.';

ALTER TABLE ad_detail_company_circle MODIFY verified_ad bool   DEFAULT false  COMMENT 'This field will decide whether this ad has been verified in this circle or not. In case the user has official_emailid_verified field in users set to true and posting an ad then this field will be true. For cases like a chartered account wants to post an ad to a circle this field will be false.';

