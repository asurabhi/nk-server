package com.nku.ads.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import com.nku.ads.domain.Team;
import com.nku.ads.repository.TeamRepository;

@Service
@Transactional(readOnly=false)
public class TeamServiceImpl implements TeamService{
	
	static final Logger LOG = LoggerFactory.getLogger(TeamServiceImpl.class);
	private final TeamRepository teamRepository;
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	public TeamServiceImpl(TeamRepository teamRepository) {
		this.teamRepository = teamRepository;
	}
	@Transactional(readOnly=true)
	public List<Team> getTeamsList() {
		return teamRepository.findAll();
	}

	public Team getTeam() {
		// print internal state
	    //LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
	    //StatusPrinter.print(lc);
		LOG.info("Kiraninfo: in getTeam");
		LOG.warn("Kiranwarn: in getTeam");

		List<Team> teams = teamRepository.findAll();
		if(!teams.isEmpty()){
			return teams.get(0);
		} else{
			return null;
		}
	}

    public List<Team> findAll(int page, int pageSize) {

	    TypedQuery<Team> query = em.createQuery("select c from Team c", Team.class);

	    query.setFirstResult(page * pageSize);
	    query.setMaxResults(pageSize);

	    return query.getResultList();
	  }
}
