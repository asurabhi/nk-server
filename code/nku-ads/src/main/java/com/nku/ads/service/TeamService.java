package com.nku.ads.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nku.ads.domain.Team;

@Transactional(readOnly=false)//transactional annotation here is optional
public interface TeamService {

	public List<Team> getTeamsList();
	public Team getTeam();
	List<Team> findAll(int page, int pageSize);
}
