package com.nku.ads.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nku.ads.domain.Team;

public interface TeamRepository extends JpaRepository<Team, Integer> {

}
