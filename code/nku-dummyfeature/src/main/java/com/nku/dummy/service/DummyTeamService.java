package com.nku.dummy.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nku.dummy.domain.DummyTeam;

@Transactional(readOnly=false)//transactional annotation here is optional
public interface DummyTeamService {

	public List<DummyTeam> getTeamsList();
	public DummyTeam getTeam();
	List<DummyTeam> findAll(int page, int pageSize);
}
