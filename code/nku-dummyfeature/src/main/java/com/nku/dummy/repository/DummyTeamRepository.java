package com.nku.dummy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nku.dummy.domain.DummyTeam;

public interface DummyTeamRepository extends JpaRepository<DummyTeam, Integer> {

}
