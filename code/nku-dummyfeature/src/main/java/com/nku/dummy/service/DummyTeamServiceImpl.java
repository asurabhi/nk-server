package com.nku.dummy.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.dummy.domain.DummyTeam;
import com.nku.dummy.repository.DummyTeamRepository;

@Service
@Transactional(readOnly=false)
public class DummyTeamServiceImpl implements DummyTeamService{
	
	static final Logger LOG = LoggerFactory.getLogger(DummyTeamServiceImpl.class);
	private final DummyTeamRepository teamRepository;
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	public DummyTeamServiceImpl(DummyTeamRepository teamRepository) {
		this.teamRepository = teamRepository;
	}
	@Transactional(readOnly=true)
	public List<DummyTeam> getTeamsList() {
		return teamRepository.findAll();
	}

	public DummyTeam getTeam() {
		// print internal state
	    //LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
	    //StatusPrinter.print(lc);
		LOG.info("Kiraninfo: in dummy getTeam");
		LOG.warn("Kiranwarn: in dummy getTeam");

		List<DummyTeam> teams = teamRepository.findAll();
		if(!teams.isEmpty()){
			return teams.get(0);
		} else{
			return null;
		}
	}

    public List<DummyTeam> findAll(int page, int pageSize) {

	    TypedQuery<DummyTeam> query = em.createQuery("select c from Team c", DummyTeam.class);

	    query.setFirstResult(page * pageSize);
	    query.setMaxResults(pageSize);

	    return query.getResultList();
	  }
}
