DROP DATABASE IF EXISTS nuk;
create database nuk;

use nuk;
SOURCE ./nku-dbconfig/sql/schema.sql;
SOURCE ./nku-dbconfig/sql/seeddata.sql;
