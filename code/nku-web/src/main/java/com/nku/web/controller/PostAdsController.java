package com.nku.web.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nku.facade.domain.AdDetailDTO;
import com.nku.facade.domain.CityDTO;
import com.nku.facade.service.PostAdsFacade;

@Component
@Path("/postad")
public class PostAdsController {
	
	private final PostAdsFacade postAdsFacade;
	@Autowired
	public PostAdsController(PostAdsFacade postAdsFacade) {
		this.postAdsFacade=postAdsFacade;
	}
	@POST
	@Path("/newAd")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postAdv3(String adDetailDTOStr) {
		//Gson gson = new Gson();
		
		Gson gson = new GsonBuilder().create();//.setDateFormat("yyyy-MM-dd'T'HHmm:ssZ").create();
		AdDetailDTO dto = gson.fromJson(adDetailDTOStr, AdDetailDTO.class);
		AdDetailDTO adDetailDTO2 =postAdsFacade.postAd(dto);
		return Response.status(200).entity(gson.toJson(adDetailDTO2)).build();
	}
}