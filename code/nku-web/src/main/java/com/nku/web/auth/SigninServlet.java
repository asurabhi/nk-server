package com.nku.web.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SigninServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {

		String clientId="459147165893-3ju9h24df8elaiqcsuruejul6bnrqkql.apps.googleusercontent.com";
		// redirect to google for authorization
		StringBuilder oauthUrl = new StringBuilder().append("https://accounts.google.com/o/oauth2/auth")
				.append("?client_id=").append(clientId) // the client id from the api console registration
				.append("&response_type=code")
				.append("&scope=openid%20email") // scope is the api permissions we are requesting
				.append("&redirect_uri=http://localhost:8080/nku-web/callback") // the servlet that google redirects to after authorization
				.append("&state=this_can_be_anything_to_help_correlate_the_response%3Dlike_session_id")
				.append("&access_type=offline") // here we are asking to access to user's data while they are not signed in
				.append("&approval_prompt=force"); // this requires them to verify which account to use, if they are already signed in

		resp.sendRedirect(oauthUrl.toString());
	} 
}