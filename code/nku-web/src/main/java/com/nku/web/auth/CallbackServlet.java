package com.nku.web.auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import com.google.common.collect.ImmutableMap;

import org.apache.http.NameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sun.jersey.api.core.HttpResponseContext;

public class CallbackServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
		// google redirects with
		//http://localhost:8089/callback?state=this_can_be_anything_to_help_correlate_the_response%3Dlike_session_id&code=4/ygE-kCdJ_pgwb1mKZq3uaTEWLUBd.slJWq1jM9mcUEnp6UAPFm0F2NQjrgwI&authuser=0&prompt=consent&session_state=a3d1eb134189705e9acf2f573325e6f30dd30ee4..d62c

		// if the user denied access, we get back an error, ex
		// error=access_denied&state=session%3Dpotatoes

		if (req.getParameter("error") != null) {
			resp.getWriter().println(req.getParameter("error"));
			return;
		}

		// google returns a code that can be exchanged for a access token
		String code = req.getParameter("code");
		String clientId="459147165893-3ju9h24df8elaiqcsuruejul6bnrqkql.apps.googleusercontent.com";
		String clientSecret = "9rbAsO_sXfuRFG-pKtPqnwd-";
		// get the access token by post to Google
		String body = post("https://accounts.google.com/o/oauth2/token", ImmutableMap.<String,String>builder()
				.put("code", code)
				.put("client_id", clientId)
				.put("client_secret", clientSecret)
				.put("redirect_uri", "http://localhost:8080/nku-web/callback")
				.put("grant_type", "authorization_code").build());

		JSONObject jsonObject = null;

		// get the access token from json and request info from Google
		try{
		jsonObject = (JSONObject) new JSONParser().parse(body);
		}
		catch(ParseException pe){
			throw new RuntimeException("Unable to parse json " + body);
		}

		// google tokens expire after an hour, but since we requested offline access we can get a new token without user involvement via the refresh token
		String accessToken = (String) jsonObject.get("access_token");

		// you may want to store the access token in session
		req.getSession().setAttribute("access_token", accessToken);

		// get some info about the user with the access token
		String json = get(new StringBuilder("https://www.googleapis.com/oauth2/v1/userinfo?access_token=").append(accessToken).toString());

		// now we could store the email address in session

		// return the json of the user's basic info
		resp.getWriter().println(json);
	}

	// makes a GET request to url and returns body as a string
	public String get(String url) throws ClientProtocolException, IOException {
		return execute(new HttpGet(url));
	}

	// makes a POST request to url with form parameters and returns body as a string
	public String post(String url, Map<String,String> formParameters) throws ClientProtocolException, IOException { 
		HttpPost request = new HttpPost(url);

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		for (String key : formParameters.keySet()) {
			nvps.add(new BasicNameValuePair(key, formParameters.get(key))); 
		}

		request.setEntity(new UrlEncodedFormEntity(nvps));

		return execute(request);
	}

	// makes request and checks response code for 200
	private String execute(HttpRequestBase request) throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = httpClient.execute(request);

		HttpEntity entity = response.getEntity();
		String body = EntityUtils.toString(entity);

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Expected 200 but got " + response.getStatusLine().getStatusCode() + ", with body " + body);
		}

		return body;
	}
}


