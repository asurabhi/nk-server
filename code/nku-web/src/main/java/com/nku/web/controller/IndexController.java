package com.nku.web.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.nku.core.domain.model.City;

@Path("/hello")
public class IndexController {
	
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) {
 
		String output = "Jersey say : " + msg;
		
		City city = new City();
		city.setCityId(10);
		city.setName(msg);
		
		Gson gson = new Gson();
		gson.toJson(city);
 
		return Response.status(200).entity(gson.toJson(city)).build();
 
	}
 
}