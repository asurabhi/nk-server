package com.nku.web.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.nku.facade.domain.StateDTO;
import com.nku.facade.service.AdminFacade;

@Component
@Path("/jsonp")
public class AdminRestController {

	@Autowired
	private AdminFacade adminFacade;

	@GET
	@Path("cities/{param}")
	public Response getCities(@PathParam("param") String state) {
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson("")).build();

	}

	@GET
	@Path("statecity")
	public Response getStatesCity() {

		List<StateDTO> allStates = adminFacade.getAllStatesCities();
		Gson gson = new Gson();
		
		String json = gson.toJson(allStates);
		return Response.status(200).entity(json).build();

	}


}
