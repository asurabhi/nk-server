package com.nku.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

	@RequestMapping(value="/hello")
	public ModelAndView goToHelloPage() {
		ModelAndView view = new ModelAndView();
		view.setViewName("hellopage"); //name of the jsp-file in the "page" folder
		
		String str = "Hello Controller";
		view.addObject("message", str); //adding of str object as "message" parameter
		
		return view;
	}
	
}
