package com.nku.web.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.nku.facade.domain.AdDetailDTO;
import com.nku.facade.domain.CatalogDTO;
import com.nku.facade.domain.CategoryDTO;
import com.nku.facade.domain.CityDTO;
import com.nku.facade.domain.LocalityDTO;
import com.nku.facade.service.BrowseAdsFacade;

@Component
@Path("/browse")
public class BrowseAdsController {
	
	private final BrowseAdsFacade browseAdsFacade;
	@Autowired
	public BrowseAdsController(BrowseAdsFacade browseAdsFacade) {
		this.browseAdsFacade=browseAdsFacade;
	}
	@GET
	@Path("/categories/{param}")
	public Response getCategories(@PathParam("param") int catalogId) { 
		List<CategoryDTO> categories =browseAdsFacade.findCategories(catalogId);
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(categories)).build();
	}
	
	@GET
	@Path("/catalogs")
	public Response getCatalogs(){
		List<CatalogDTO> catalogs =browseAdsFacade.findAllCatalogs();
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(catalogs)).build();
	}
	
	@GET
	@Path("/cities")
	public Response getCities(){
		List<CityDTO> cities =browseAdsFacade.findAllCities();
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(cities)).build();
	}
	
	@GET
	@Path("/localities/{param}")
	public Response getCategories(@PathParam("param") Integer cityId) { 
		List<LocalityDTO> localityDTOs =browseAdsFacade.findLocalitiesByCity(cityId);
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(localityDTOs)).build();
	}
	
	@GET
	@Path("/ads/{param}")
	public Response getAds(@PathParam("param")int categoryId){
		List<AdDetailDTO> catalogs =browseAdsFacade.findAds(categoryId);
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(catalogs)).build();
	}
	
	@GET
	@Path("/addetail/{param}")
	public Response getAdDetail(@PathParam("param")int adDetailId){
		AdDetailDTO adDetailDTO =browseAdsFacade.findAdDetail(adDetailId);
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(adDetailDTO)).build();
	}
 
}