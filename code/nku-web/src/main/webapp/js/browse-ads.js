var app = angular.module("classifiedsv2", []);
app.controller("browseAdsController", function($scope,$http) {
   // $scope.message = "";
    $scope.listCatalogs = function() {
    	console.log("try loading catalogs");
    	$http.get('rest/browse/catalogs')
    	.success(function(data) {
    		$scope.catalogs = data;				
    		console.log('Catalogs data Loaded successfully');
    	});
    },
    $scope.listCategories = function($catalogId) {
    	console.log("try loading categories");
    	$http.get('rest/browse/categories/'+$catalogId)
    	.success(function(data) {
    		$scope.categories = data;				
    		console.log('Categories data Loaded successfully');
    	});
    },
    
    $scope.listAds = function($categoryId) {
    	console.log("try loading ads");
    	$http.get('rest/browse/ads/'+$categoryId)
    	.success(function(data) {
    		$scope.ads = data;				
    		console.log('Ads data Loaded successfully');
    	});
    }
    
});
