// start of init
var classifieds = angular.module('classifieds', [ 'ngRoute',
		'classifiedsController', 'addCirclesController',
		'showCirclesController', 'selectCityController' ]);

$(function() {
	FastClick.attach(document.body);
});

classifieds.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when("/home", {
		templateUrl : "views/home.html"
	}).when("/AddCircles", {
		templateUrl : "views/AddCircles.html"
	}).when("/ShowCircles", {
		templateUrl : "views/ShowCircles.html"
	}).when("/SelectCity", {
		templateUrl : "views/SelectCity.html"
	}).otherwise({
		redirectTo : '/home'
	});
} ]);

var classifiedsController = angular.module('classifiedsController', []);
var addCirclesController = angular.module('addCirclesController', []);
var showCirclesController = angular.module('showCirclesController', []);
var selectCityController = angular.module('selectCityController', []);

addCirclesController.controller('addCirclesController', function($scope) {
	//console.log('inside AddCircleController');
	$scope.message = 'this is the addCircles controller scope';
	$scope.clickImage =  function(){
			console.log('inside the scope of the controller');
	};
});

selectCityController.controller('selectCityController',
		function($scope, $http) {
			var locations = {
				"ResponseCode" : 0,
				"ResponseMessage" : "OK",
				"ResponseDateTime" : "4/13/2015 8:34:07 AM GMT",
				"Data" : [ {
					"ID" : "1",
					"Name" : "Assam ",
					"Type" : "state"
				}, {
					"ID" : "2",
					"Name" : "Jammu and Kashmir",
					"Type" : "state"
				}, {
					"ID" : "3",
					"Name" : "Maharashtra",
					"Type" : "state"
				}, {
					"ID" : "4",
					"Name" : "Uttar Pradesh",
					"Type" : "state"
				}, {
					"ID" : "5",
					"Name" : "Gujarat",
					"Type" : "state"
				}, {
					"ID" : "6",
					"Name" : "Andhra Pradesh",
					"Type" : "state"
				}, {
					"ID" : "7",
					"Name" : "Karnataka",
					"Type" : "state"
				} ]
			};

			/**console.log('inside selectCityController');
			$scope.message = 'this is the selectCityController scope';
			console.log(locations.Data);
			$scope.displayCities = locations.Data; **/
			$http({
				method : 'GET',
				url : "rest/jsonp/states"
			}).success(function(data) {
				//console.log('inside the cities list');
				//console.log(data);
				$scope.displayCities = data;
				//console.log('fetching cities');
			});

		});



classifiedsController.controller('classifiedsController', function($scope,
		$http, catalogService) {
	var categories = {
		"ResponseCode" : 0,
		"ResponseMessage" : "OK",
		"ResponseDateTime" : "4/13/2015 8:34:07 AM GMT",
		"Data" : [ {
			"ID" : "1",
			"Name" : "Mobiles and Tablets",
			"Type" : "mobiles",
			"BrowseData" : [ {
				"ID" : "1",
				"Name" : "All Mobile Phones and Tablets",
				"Type" : "mobiles"
			}, {
				"ID" : "2",
				"Name" : "Mobile Phone",
				"Type" : "electronics"
			}, {
				"ID" : "3",
				"Name" : "Tablets",
				"Type" : "vehicle"
			}, {
				"ID" : "4",
				"Name" : "Accessories",
				"Type" : "furniture"
			} ]
		}, {
			"ID" : "2",
			"Name" : "Electronics and Computers",
			"Type" : "electronics",
			"BrowseData" : [ {
				"ID" : "1",
				"Name" : "All Electronics and Computers",
				"Type" : "mobiles"
			}, {
				"ID" : "2",
				"Name" : "Computer",
				"Type" : "electronics"
			}, {
				"ID" : "3",
				"Name" : "Laptops",
				"Type" : "vehicle"
			}, {
				"ID" : "4",
				"Name" : "TV",
				"Type" : "furniture"
			} , {
				"ID" : "5",
				"Name" : "Camera & Accessories",
				"Type" : "furniture"
			}]
		}, {
			"ID" : "3",
			"Name" : "Vehicles",
			"Type" : "vehicle",
			"BrowseData" : [ {
				"ID" : "1",
				"Name" : "All Vehicles",
				"Type" : "mobiles"
			}, {
				"ID" : "2",
				"Name" : "Cars",
				"Type" : "electronics"
			}, {
				"ID" : "3",
				"Name" : "Motorcycles",
				"Type" : "vehicle"
			}, {
				"ID" : "4",
				"Name" : "Commercial Vehicles",
				"Type" : "furniture"
			} , {
				"ID" : "5",
				"Name" : "Spare Parts and Accessories",
				"Type" : "furniture"
			}]
		}, {
			"ID" : "4",
			"Name" : "Home and Furniture",
			"Type" : "furniture",
			"BrowseData" : [ {
				"ID" : "1",
				"Name" : "All Home and Furniture",
				"Type" : "mobiles"
			}, {
				"ID" : "2",
				"Name" : "Furniture",
				"Type" : "electronics"
			}, {
				"ID" : "3",
				"Name" : "Decor & Furnishing",
				"Type" : "vehicle"
			}, {
				"ID" : "4",
				"Name" : "Home and Kitchen Appliances",
				"Type" : "furniture"
			} , {
				"ID" : "5",
				"Name" : "Handicraft and others",
				"Type" : "furniture"
			}]
		}, {
			"ID" : "5",
			"Name" : "Animals",
			"Type" : "animal",
			"BrowseData" : [ {
				"ID" : "1",
				"Name" : "All Animals",
				"Type" : "mobiles"
			}, {
				"ID" : "2",
				"Name" : "Dogs",
				"Type" : "electronics"
			}, {
				"ID" : "3",
				"Name" : "Cats",
				"Type" : "vehicle"
			}, {
				"ID" : "4",
				"Name" : "Pet Food and Accessories",
				"Type" : "furniture"
			} , {
				"ID" : "5",
				"Name" : "Other Pets",
				"Type" : "furniture"
			}]
		}, {
			"ID" : "6",
			"Name" : "Real Estate",
			"Type" : "estate",
			"BrowseData" : [ {
				"ID" : "1",
				"Name" : "All Real Estate",
				"Type" : "mobiles"
			}, {
				"ID" : "2",
				"Name" : "Apartments",
				"Type" : "electronics"
			}, {
				"ID" : "3",
				"Name" : "Houses",
				"Type" : "vehicle"
			}, {
				"ID" : "4",
				"Name" : "PG & Roomates",
				"Type" : "furniture"
			} , {
				"ID" : "5",
				"Name" : "Other Real Estate",
				"Type" : "furniture"
			}]
		}, {
			"ID" : "7",
			"Name" : "Kids Products",
			"Type" : "kids",
			"BrowseData" : [ {
				"ID" : "1",
				"Name" : "All Kids Products",
				"Type" : "mobiles"
			}, {
				"ID" : "2",
				"Name" : "Clothes and Footwear",
				"Type" : "electronics"
			}, {
				"ID" : "3",
				"Name" : "Toys and Games",
				"Type" : "vehicle"
			}, {
				"ID" : "4",
				"Name" : "Nutrition",
				"Type" : "furniture"
			} , {
				"ID" : "5",
				"Name" : "Other Kids Products",
				"Type" : "furniture"
			}]
		} ]

	};
	
	$scope.displayCategories = categories.Data;
	
	$scope.getCategory = function(categoryBrowseData) {
		catalogService.saveCategoryID(
				categoryBrowseData);
	};

});

showCirclesController.controller('showCirclesController', function($scope,
		catalogService) {

	//console.log('inside selectCategoryController');
	
	$scope.displaySubData = catalogService.getCategoryID();
	//sole.log(catalogService.getCategoryID());
	
});

// END of init
// register other stuff..
classifieds.factory('catalogService', function($http) {
	var categoryID;
	var catService = {};


		catService.listCities = function(city, callback) {
			$http({
				method : 'GET',
				url : "rest/hello/cities",
				method : "GET",
			}).success(callback)
		};
		
		catService.saveCategoryID = function(Id){
			categoryID = Id;
			//console.log("inside factory scope " + categoryID);
			
		};
		catService.getCategoryID = function(){
			//console.log('inside the getCategoryID');
			return categoryID;
		};
		
		return catService;

});