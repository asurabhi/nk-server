var classifiedsModule = angular.module('ClassifiedsV2',['ngRoute']);

classifiedsModule.controller('catalogsController', function($scope, $routeParams,classifiedService) {
	classifiedService.getCatalogsData().success(function(response) {
        $scope.catalogs = response;
    });
});

classifiedsModule.controller('categoriesController', function($scope, $routeParams, classifiedService) {
	$scope.catalogId = $routeParams.catalogId;
	classifiedService.getCategoriesData($scope.catalogId).success(function(response) {
        $scope.categories = response;
    });
});
classifiedsModule.controller('adsController', function($scope, $routeParams, classifiedService) {
	$scope.categoryId = $routeParams.categoryId;
	classifiedService.getAdsData($scope.categoryId).success(function(response) {
        $scope.ads = response;
    });
});

classifiedsModule.controller('adDetailController', function($scope, $routeParams, classifiedService) {
	$scope.adDetailId = $routeParams.adDetailId;
	classifiedService.getAdDetailData($scope.adDetailId).success(function(response) {
        $scope.adDetail = response;
    });
});

classifiedsModule.controller('defineAdController', function($scope, $routeParams, classifiedService) {
	//We need to populate city,categories etc.. here
	classifiedService.getCatalogsData().success(function(response) {
        $scope.catalogs = response;
        $scope.selectedCatalog= $scope.catalogs[0];
        classifiedService.getCategoriesData($scope.selectedCatalog.catalogId).success(function(response) {
	        $scope.categories = response;
	    });
	});
	classifiedService.getCitiesData().success(function(response) {
        $scope.cities = response;
        $scope.selectedCity= $scope.cities[0];
        classifiedService.getLocalitiesData($scope.selectedCity.cityId).success(function(response) {
	        $scope.localities = response;
	    });
	});
	$scope.showCategories=function(catalogId) {
		classifiedService.getCategoriesData(catalogId).success(function(response) {
	        $scope.categories = response;
	    });
	};
	$scope.showLocalities=function(cityId) {
		classifiedService.getLocalitiesData(cityId).success(function(response) {
	        $scope.localities = response;
	    });
	}
});
classifiedsModule.controller("postAdController", function($scope,$http) {
	$scope.postNewAd = function() {
		var dataObj = {
				name : $scope.newAdForm.name,
				categoryId : $scope.selectedCategory.categoryId,
				userId : 1,
				cityId : $scope.selectedCity.cityId,
				localityId : $scope.selectedLocality.localityId,
				adressline1:$scope.adressline1,
				contactNumber:$scope.contactNumber,
				description:$scope.description,
				officialEmailid:$scope.officialEmailid,
				personalEmailid:$scope.personalEmailid,
				personalMailidVisible:$scope.personalMailidVisible,
				priceNegotiable:$scope.priceNegotiable,
				price:$scope.price					
		};
		console.log("Posting ad v2");
		$http({
            method: 'POST',
            url: 'rest/postad/newAd',
            data:dataObj,
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data) {
			console.log("Response from post adv2"+data);
		});
		/*$http.post('rest/postad/newAd',dataObj)
		.success(function(data) {
			console.log("Response from post ad"+data);
		});*/
	}	    
});

classifiedsModule.factory('classifiedService', function($http) {
   var classifiedServiceProvider = {};
   classifiedServiceProvider.getCatalogsData = function() {
        return $http({
            method: 'GET',
            url: 'rest/browse/catalogs',
            cache: 'false'
        });
    }
   classifiedServiceProvider.getCitiesData = function() {
       return $http({
           method: 'GET',
           url: 'rest/browse/cities',
           cache: 'false'
       });
   }
   classifiedServiceProvider.getCategoriesData = function(catalogId) {
        return $http({
            method: 'GET',
            url: 'rest/browse/categories/'+catalogId,
            cache: 'false'
        });
    }
   
   classifiedServiceProvider.getLocalitiesData = function(cityId) {
       return $http({
           method: 'GET',
           url: 'rest/browse/localities/'+cityId,
           cache: 'false'
       });
   }

   classifiedServiceProvider.getAdsData = function(categoryId) {
    	 return $http({
             method: 'GET',
             url: 'rest/browse/ads/'+categoryId,
             cache: 'false'
         });
    }
   
   classifiedServiceProvider.getAdDetailData = function(adDetailId) {
  	 return $http({
           method: 'GET',
           url: 'rest/browse/addetail/'+adDetailId,
           cache: 'false'
       });
  }

    return classifiedServiceProvider;
});

classifiedsModule.config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when("/catalogs", {templateUrl: "views_v2/catalogs.html", controller: "catalogsController"}).
            when("/categories/:catalogId", {templateUrl: "views_v2/categories.html", controller: "categoriesController"}).
            when("/ads/:categoryId", {templateUrl: "views_v2/ads.html", controller: "adsController"}).
            when("/addetail/:adDetailId", {templateUrl: "views_v2/adDetail.html", controller: "adDetailController"}).
            when("/postadform", {templateUrl: "views_v2/postadform.html", controller: "defineAdController"}).

            otherwise({redirectTo: '/home'});
    }]);


classifiedsModule.directive('numbersOnly', function(){
	   return {
	     require: 'ngModel',
	     link: function(scope, element, attrs, modelCtrl) {
	       modelCtrl.$parsers.push(function (inputValue) {
	           // this next if is necessary for when using ng-required on your input. 
	           // In such cases, when a letter is typed first, this parser will be called
	           // again, and the 2nd time, the value will be undefined
	           if (inputValue == undefined) return '' 
	           var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
	           if (transformedInput!=inputValue) {
	              modelCtrl.$setViewValue(transformedInput);
	              modelCtrl.$render();
	           }         

	           return transformedInput;         
	       });
	     }
	   };
	});