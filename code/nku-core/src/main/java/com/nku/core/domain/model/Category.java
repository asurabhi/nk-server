package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the categories database table.
 * 
 */
@Entity
@Table(name="categories")
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="category_id")
	private Integer categoryId;

	private String name;

	//bi-directional many-to-one association to AdDetail
	@OneToMany(mappedBy="category")
	private List<AdDetail> adDetails;

	//bi-directional many-to-one association to Catalog
	@ManyToOne
	@JoinColumn(name="catalog_id")
	private Catalog catalog;

	//bi-directional many-to-one association to CategoryAttribute
	@OneToMany(mappedBy="category")
	private List<CategoryAttribute> categoryAttributes;

	public Category() {
	}

	public Integer getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AdDetail> getAdDetails() {
		return this.adDetails;
	}

	public void setAdDetails(List<AdDetail> adDetails) {
		this.adDetails = adDetails;
	}

	public AdDetail addAdDetail(AdDetail adDetail) {
		getAdDetails().add(adDetail);
		adDetail.setCategory(this);

		return adDetail;
	}

	public AdDetail removeAdDetail(AdDetail adDetail) {
		getAdDetails().remove(adDetail);
		adDetail.setCategory(null);

		return adDetail;
	}

	public Catalog getCatalog() {
		return this.catalog;
	}

	public void setCatalog(Catalog catalog) {
		this.catalog = catalog;
	}

	public List<CategoryAttribute> getCategoryAttributes() {
		return this.categoryAttributes;
	}

	public void setCategoryAttributes(List<CategoryAttribute> categoryAttributes) {
		this.categoryAttributes = categoryAttributes;
	}

	public CategoryAttribute addCategoryAttribute(CategoryAttribute categoryAttribute) {
		getCategoryAttributes().add(categoryAttribute);
		categoryAttribute.setCategory(this);

		return categoryAttribute;
	}

	public CategoryAttribute removeCategoryAttribute(CategoryAttribute categoryAttribute) {
		getCategoryAttributes().remove(categoryAttribute);
		categoryAttribute.setCategory(null);

		return categoryAttribute;
	}

}