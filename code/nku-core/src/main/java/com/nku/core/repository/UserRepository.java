package com.nku.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nku.core.domain.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
