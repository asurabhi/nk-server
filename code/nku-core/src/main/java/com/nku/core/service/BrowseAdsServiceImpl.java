package com.nku.core.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.AdDetail;
import com.nku.core.domain.model.Catalog;
import com.nku.core.domain.model.Category;
import com.nku.core.domain.model.Locality;
import com.nku.core.repository.AdDetailReposiory;
import com.nku.core.repository.CatalogRepository;
import com.nku.core.repository.CategoryRepository;
import com.nku.core.repository.LocalityReposiory;

@Service
@Transactional(readOnly=false)
public class BrowseAdsServiceImpl implements BrowseAdsService{

	private final AdDetailReposiory adDetailReposiory;
	private final CatalogRepository catalogRepository;
	private final CategoryRepository categoryRepository;
	private final LocalityReposiory localityReposiory;
	@Autowired
	public BrowseAdsServiceImpl(CatalogRepository catalogRepository, CategoryRepository categoryRepository
			,AdDetailReposiory adDetailReposiory,LocalityReposiory localityReposiory){
		this.catalogRepository = catalogRepository;
		this.categoryRepository = categoryRepository;
		this.adDetailReposiory = adDetailReposiory;
		this.localityReposiory=localityReposiory;
	}
	public List<Catalog> findCatalogs() {
		return catalogRepository.findAll();
	}
	public List<Category> findCategories(int catalogId){
		return categoryRepository.findByCatalogCatalogId(catalogId);
	}
	
	public List<AdDetail> findAds(int categoryId) {
		List<AdDetail> adDetails = adDetailReposiory.findAll();
		
		/*
		List<AdDetail> ads = new ArrayList<AdDetail>();
		ads.add(buildAdDetail(1,"a1"));
		ads.add(buildAdDetail(2, "a2"));
		ads.add(buildAdDetail(2, "a3"));*/

		return adDetails;
	}
	private AdDetail buildAdDetail(int id, String name) {
		AdDetail ad = new AdDetail();
		ad.setAdDetailId(id);
		ad.setName(name);
		return ad;
	}
	@Override
	public List<Locality> findLocalitiesOfCity(Integer cityId) {
		return localityReposiory.findByCityCityId(cityId);
	}
	@Override
	public AdDetail findAdDetail(Integer adDetailId) {
		return adDetailReposiory.findOne(adDetailId);
	}

}