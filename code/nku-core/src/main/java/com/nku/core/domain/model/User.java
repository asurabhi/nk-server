package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	private Integer userId;

	@Column(name="contact_number")
	private int contactNumber;

	private String emailid;

	@Column(name="is_system_generated")
	private byte isSystemGenerated;

	@Column(name="is_third_party_login")
	private byte isThirdPartyLogin;

	private String name;

	@Column(name="official_emailid")
	private String officialEmailid;

	@Column(name="official_emailid_verified")
	private byte officialEmailidVerified;

	private String password;

	//bi-directional many-to-one association to AdDetail
	@OneToMany(mappedBy="user")
	private List<AdDetail> adDetails;

	public User() {
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public int getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(int contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public byte getIsSystemGenerated() {
		return this.isSystemGenerated;
	}

	public void setIsSystemGenerated(byte isSystemGenerated) {
		this.isSystemGenerated = isSystemGenerated;
	}

	public byte getIsThirdPartyLogin() {
		return this.isThirdPartyLogin;
	}

	public void setIsThirdPartyLogin(byte isThirdPartyLogin) {
		this.isThirdPartyLogin = isThirdPartyLogin;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfficialEmailid() {
		return this.officialEmailid;
	}

	public void setOfficialEmailid(String officialEmailid) {
		this.officialEmailid = officialEmailid;
	}

	public byte getOfficialEmailidVerified() {
		return this.officialEmailidVerified;
	}

	public void setOfficialEmailidVerified(byte officialEmailidVerified) {
		this.officialEmailidVerified = officialEmailidVerified;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<AdDetail> getAdDetails() {
		return this.adDetails;
	}

	public void setAdDetails(List<AdDetail> adDetails) {
		this.adDetails = adDetails;
	}

	public AdDetail addAdDetail(AdDetail adDetail) {
		getAdDetails().add(adDetail);
		adDetail.setUser(this);

		return adDetail;
	}

	public AdDetail removeAdDetail(AdDetail adDetail) {
		getAdDetails().remove(adDetail);
		adDetail.setUser(null);

		return adDetail;
	}

}