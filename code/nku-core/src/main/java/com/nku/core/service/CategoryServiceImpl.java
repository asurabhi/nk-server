package com.nku.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.Category;
import com.nku.core.repository.CategoryRepository;
@Service
@Transactional(readOnly=false)
public class CategoryServiceImpl implements CategoryService{

	private CategoryRepository categoryRepository;
	@Autowired
	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		this.categoryRepository=categoryRepository;
	}
	@Override
	public Category findCategoryById(Integer id) {
		return categoryRepository.findOne(id);
	}

}
