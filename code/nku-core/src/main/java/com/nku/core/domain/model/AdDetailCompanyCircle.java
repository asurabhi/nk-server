package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ad_detail_company_circle database table.
 * 
 */
@Entity
@Table(name="ad_detail_company_circle")
@NamedQuery(name="AdDetailCompanyCircle.findAll", query="SELECT a FROM AdDetailCompanyCircle a")
public class AdDetailCompanyCircle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ad_detail_company_circle_id")
	private Integer adDetailCompanyCircleId;

	@Column(name="verified_ad")
	private byte verifiedAd;

	//bi-directional many-to-one association to CompanyCircle
	@ManyToOne
	@JoinColumn(name="company_circle_id")
	private CompanyCircle companyCircle;

	//bi-directional many-to-one association to AdDetail
	@ManyToOne
	@JoinColumn(name="ad_detail_id")
	private AdDetail adDetail;

	public AdDetailCompanyCircle() {
	}

	public Integer getAdDetailCompanyCircleId() {
		return this.adDetailCompanyCircleId;
	}

	public void setAdDetailCompanyCircleId(Integer adDetailCompanyCircleId) {
		this.adDetailCompanyCircleId = adDetailCompanyCircleId;
	}

	public byte getVerifiedAd() {
		return this.verifiedAd;
	}

	public void setVerifiedAd(byte verifiedAd) {
		this.verifiedAd = verifiedAd;
	}

	public CompanyCircle getCompanyCircle() {
		return this.companyCircle;
	}

	public void setCompanyCircle(CompanyCircle companyCircle) {
		this.companyCircle = companyCircle;
	}

	public AdDetail getAdDetail() {
		return this.adDetail;
	}

	public void setAdDetail(AdDetail adDetail) {
		this.adDetail = adDetail;
	}

}