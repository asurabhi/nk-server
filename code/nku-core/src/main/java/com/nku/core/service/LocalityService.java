package com.nku.core.service;

import java.util.List;

import com.nku.core.domain.model.Locality;

public interface LocalityService {

	List<Locality> findLocalitiesByCityId(Long cityId);
}
