package com.nku.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nku.core.domain.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

	List<Category> findByCatalogCatalogId(int catalogId);

}
