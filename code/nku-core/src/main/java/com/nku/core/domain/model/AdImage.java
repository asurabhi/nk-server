package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ad_images database table.
 * 
 */
@Entity
@Table(name="ad_images")
@NamedQuery(name="AdImage.findAll", query="SELECT a FROM AdImage a")
public class AdImage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ad_image_id")
	private Integer adImageId;

	@Column(name="image_file_path")
	private String imageFilePath;

	//bi-directional many-to-one association to AdDetail
	@ManyToOne
	@JoinColumn(name="ad_detail_id")
	private AdDetail adDetail;

	public AdImage() {
	}

	public Integer getAdImageId() {
		return this.adImageId;
	}

	public void setAdImageId(Integer adImageId) {
		this.adImageId = adImageId;
	}

	public String getImageFilePath() {
		return this.imageFilePath;
	}

	public void setImageFilePath(String imageFilePath) {
		this.imageFilePath = imageFilePath;
	}

	public AdDetail getAdDetail() {
		return this.adDetail;
	}

	public void setAdDetail(AdDetail adDetail) {
		this.adDetail = adDetail;
	}

}