package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ad_details database table.
 * 
 */
@Entity
@Table(name="ad_details")
@NamedQuery(name="AdDetail.findAll", query="SELECT a FROM AdDetail a")
public class AdDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ad_detail_id")
	private Integer adDetailId;

	private String adressline1;

	@Column(name="contact_number")
	private Long contactNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_ts")
	private Date createdTs;

	private String description;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_modified_ts")
	private Date lastModifiedTs;

	private String name;

	@Column(name="official_emailid")
	private String officialEmailid;

	@Column(name="other_locality")
	private String otherLocality;

	@Column(name="personal_mailid")
	private String personalMailid;

	@Column(name="personal_mailid_visible")
	private Boolean personalMailidVisible;

	private Double price;

	@Column(name="price_negotiable")
	private Boolean priceNegotiable;

	private int priority;

	@Column(name="search_tags_csv")
	private String searchTagsCsv;

	private String status;

	//bi-directional many-to-one association to AdDetailCompanyCircle
	@OneToMany(mappedBy="adDetail")
	private List<AdDetailCompanyCircle> adDetailCompanyCircles;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="city_id")
	private City city;

	//bi-directional many-to-one association to Locality
	@ManyToOne
	@JoinColumn(name="locality_id")
	private Locality locality;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="category_id")
	private Category category;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	//bi-directional many-to-one association to AdImage
	@OneToMany(mappedBy="adDetail")
	private List<AdImage> adImages;

	//bi-directional many-to-one association to CategoryAttributeInstance
	@OneToMany(mappedBy="adDetail")
	private List<CategoryAttributeInstance> categoryAttributeInstances;

	public AdDetail() {
	}
	
	public Integer getAdDetailId() {
		return adDetailId;
	}

	public void setAdDetailId(Integer adDetailId) {
		this.adDetailId = adDetailId;
	}

	public String getAdressline1() {
		return this.adressline1;
	}

	public void setAdressline1(String adressline1) {
		this.adressline1 = adressline1;
	}

	public Long getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Date getCreatedTs() {
		return this.createdTs;
	}

	public void setCreatedTs(Date createdTs) {
		this.createdTs = createdTs;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastModifiedTs() {
		return this.lastModifiedTs;
	}

	public void setLastModifiedTs(Date lastModifiedTs) {
		this.lastModifiedTs = lastModifiedTs;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfficialEmailid() {
		return this.officialEmailid;
	}

	public void setOfficialEmailid(String officialEmailid) {
		this.officialEmailid = officialEmailid;
	}

	public String getOtherLocality() {
		return this.otherLocality;
	}

	public void setOtherLocality(String otherLocality) {
		this.otherLocality = otherLocality;
	}

	public String getPersonalMailid() {
		return this.personalMailid;
	}

	public void setPersonalMailid(String personalMailid) {
		this.personalMailid = personalMailid;
	}

	public Boolean getPersonalMailidVisible() {
		return this.personalMailidVisible;
	}

	public void setPersonalMailidVisible(Boolean personalMailidVisible) {
		this.personalMailidVisible = personalMailidVisible;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getPriceNegotiable() {
		return this.priceNegotiable;
	}

	public void setPriceNegotiable(Boolean priceNegotiable) {
		this.priceNegotiable = priceNegotiable;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getSearchTagsCsv() {
		return this.searchTagsCsv;
	}

	public void setSearchTagsCsv(String searchTagsCsv) {
		this.searchTagsCsv = searchTagsCsv;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<AdDetailCompanyCircle> getAdDetailCompanyCircles() {
		return this.adDetailCompanyCircles;
	}

	public void setAdDetailCompanyCircles(List<AdDetailCompanyCircle> adDetailCompanyCircles) {
		this.adDetailCompanyCircles = adDetailCompanyCircles;
	}

	public AdDetailCompanyCircle addAdDetailCompanyCircle(AdDetailCompanyCircle adDetailCompanyCircle) {
		getAdDetailCompanyCircles().add(adDetailCompanyCircle);
		adDetailCompanyCircle.setAdDetail(this);

		return adDetailCompanyCircle;
	}

	public AdDetailCompanyCircle removeAdDetailCompanyCircle(AdDetailCompanyCircle adDetailCompanyCircle) {
		getAdDetailCompanyCircles().remove(adDetailCompanyCircle);
		adDetailCompanyCircle.setAdDetail(null);

		return adDetailCompanyCircle;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Locality getLocality() {
		return this.locality;
	}

	public void setLocality(Locality locality) {
		this.locality = locality;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<AdImage> getAdImages() {
		return this.adImages;
	}

	public void setAdImages(List<AdImage> adImages) {
		this.adImages = adImages;
	}

	public AdImage addAdImage(AdImage adImage) {
		getAdImages().add(adImage);
		adImage.setAdDetail(this);

		return adImage;
	}

	public AdImage removeAdImage(AdImage adImage) {
		getAdImages().remove(adImage);
		adImage.setAdDetail(null);

		return adImage;
	}

	public List<CategoryAttributeInstance> getCategoryAttributeInstances() {
		return this.categoryAttributeInstances;
	}

	public void setCategoryAttributeInstances(List<CategoryAttributeInstance> categoryAttributeInstances) {
		this.categoryAttributeInstances = categoryAttributeInstances;
	}

	public CategoryAttributeInstance addCategoryAttributeInstance(CategoryAttributeInstance categoryAttributeInstance) {
		getCategoryAttributeInstances().add(categoryAttributeInstance);
		categoryAttributeInstance.setAdDetail(this);

		return categoryAttributeInstance;
	}

	public CategoryAttributeInstance removeCategoryAttributeInstance(CategoryAttributeInstance categoryAttributeInstance) {
		getCategoryAttributeInstances().remove(categoryAttributeInstance);
		categoryAttributeInstance.setAdDetail(null);

		return categoryAttributeInstance;
	}

}