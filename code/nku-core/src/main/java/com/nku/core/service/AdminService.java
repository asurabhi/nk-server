package com.nku.core.service;

import java.util.List;

import com.nku.core.domain.model.City;
import com.nku.core.domain.model.State;

public interface AdminService {

	List<State> getAllStates();

	List<City> getCities(State state);
}
