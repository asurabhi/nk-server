package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="city_id")
	private Integer cityId;

	private String name;

	private Integer tier;

	//bi-directional many-to-one association to AdDetail
	@OneToMany(mappedBy="city")
	private List<AdDetail> adDetails;

	//bi-directional many-to-one association to State
	@ManyToOne
	@JoinColumn(name="state_id")
	private State state;

	//bi-directional many-to-one association to Locality
	@OneToMany(mappedBy="city")
	private List<Locality> localities;

	public City() {
	}

	public Integer getCityId() {
		return this.cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTier() {
		return tier;
	}

	public void setTier(Integer tier) {
		this.tier = tier;
	}

	public List<AdDetail> getAdDetails() {
		return this.adDetails;
	}

	public void setAdDetails(List<AdDetail> adDetails) {
		this.adDetails = adDetails;
	}

	public AdDetail addAdDetail(AdDetail adDetail) {
		getAdDetails().add(adDetail);
		adDetail.setCity(this);

		return adDetail;
	}

	public AdDetail removeAdDetail(AdDetail adDetail) {
		getAdDetails().remove(adDetail);
		adDetail.setCity(null);

		return adDetail;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<Locality> getLocalities() {
		return this.localities;
	}

	public void setLocalities(List<Locality> localities) {
		this.localities = localities;
	}

	public Locality addLocality(Locality locality) {
		getLocalities().add(locality);
		locality.setCity(this);

		return locality;
	}

	public Locality removeLocality(Locality locality) {
		getLocalities().remove(locality);
		locality.setCity(null);

		return locality;
	}

}