package com.nku.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nku.core.domain.model.Catalog;

public interface CatalogRepository extends JpaRepository<Catalog, Integer> {

}
