package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the category_attribute_options database table.
 * 
 */
@Entity
@Table(name="category_attribute_options")
@NamedQuery(name="CategoryAttributeOption.findAll", query="SELECT c FROM CategoryAttributeOption c")
public class CategoryAttributeOption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="category_attribute_option_id")
	private Integer categoryAttributeOptionId;

	@Column(name="serial_number")
	private int serialNumber;

	private String value;

	//bi-directional many-to-one association to CategoryAttributeOptionInstance
	@OneToMany(mappedBy="categoryAttributeOption")
	private List<CategoryAttributeOptionInstance> categoryAttributeOptionInstances;

	//bi-directional many-to-one association to CategoryAttribute
	@ManyToOne
	@JoinColumn(name="category_attribute_id")
	private CategoryAttribute categoryAttribute;

	public CategoryAttributeOption() {
	}

	public Integer getCategoryAttributeOptionId() {
		return this.categoryAttributeOptionId;
	}

	public void setCategoryAttributeOptionId(Integer categoryAttributeOptionId) {
		this.categoryAttributeOptionId = categoryAttributeOptionId;
	}

	public int getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<CategoryAttributeOptionInstance> getCategoryAttributeOptionInstances() {
		return this.categoryAttributeOptionInstances;
	}

	public void setCategoryAttributeOptionInstances(List<CategoryAttributeOptionInstance> categoryAttributeOptionInstances) {
		this.categoryAttributeOptionInstances = categoryAttributeOptionInstances;
	}

	public CategoryAttributeOptionInstance addCategoryAttributeOptionInstance(CategoryAttributeOptionInstance categoryAttributeOptionInstance) {
		getCategoryAttributeOptionInstances().add(categoryAttributeOptionInstance);
		categoryAttributeOptionInstance.setCategoryAttributeOption(this);

		return categoryAttributeOptionInstance;
	}

	public CategoryAttributeOptionInstance removeCategoryAttributeOptionInstance(CategoryAttributeOptionInstance categoryAttributeOptionInstance) {
		getCategoryAttributeOptionInstances().remove(categoryAttributeOptionInstance);
		categoryAttributeOptionInstance.setCategoryAttributeOption(null);

		return categoryAttributeOptionInstance;
	}

	public CategoryAttribute getCategoryAttribute() {
		return this.categoryAttribute;
	}

	public void setCategoryAttribute(CategoryAttribute categoryAttribute) {
		this.categoryAttribute = categoryAttribute;
	}

}