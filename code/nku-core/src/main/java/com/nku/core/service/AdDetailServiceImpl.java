package com.nku.core.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.AdDetail;
import com.nku.core.repository.AdDetailReposiory;

@Service
@Transactional(readOnly=false)
public class AdDetailServiceImpl implements AdDetailService {

	private final AdDetailReposiory adDetailReposiory;
	@Autowired
	public AdDetailServiceImpl(AdDetailReposiory adDetailReposiory){
		this.adDetailReposiory = adDetailReposiory;
	}
	@Transactional(readOnly=false)
	public AdDetail save(AdDetail adDetail) {
		adDetail.setCreatedTs(new Date());
		return adDetailReposiory.save(adDetail);
	}
}
