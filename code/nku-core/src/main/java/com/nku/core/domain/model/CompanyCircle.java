package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the company_circles database table.
 * 
 */
@Entity
@Table(name="company_circles")
@NamedQuery(name="CompanyCircle.findAll", query="SELECT c FROM CompanyCircle c")
public class CompanyCircle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="company_circle_id")
	private Integer companyCircleId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="domain_name")
	private String domainName;

	private String name;

	//bi-directional many-to-one association to AdDetailCompanyCircle
	@OneToMany(mappedBy="companyCircle")
	private List<AdDetailCompanyCircle> adDetailCompanyCircles;

	public CompanyCircle() {
	}

	public Integer getCompanyCircleId() {
		return this.companyCircleId;
	}

	public void setCompanyCircleId(Integer companyCircleId) {
		this.companyCircleId = companyCircleId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDomainName() {
		return this.domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AdDetailCompanyCircle> getAdDetailCompanyCircles() {
		return this.adDetailCompanyCircles;
	}

	public void setAdDetailCompanyCircles(List<AdDetailCompanyCircle> adDetailCompanyCircles) {
		this.adDetailCompanyCircles = adDetailCompanyCircles;
	}

	public AdDetailCompanyCircle addAdDetailCompanyCircle(AdDetailCompanyCircle adDetailCompanyCircle) {
		getAdDetailCompanyCircles().add(adDetailCompanyCircle);
		adDetailCompanyCircle.setCompanyCircle(this);

		return adDetailCompanyCircle;
	}

	public AdDetailCompanyCircle removeAdDetailCompanyCircle(AdDetailCompanyCircle adDetailCompanyCircle) {
		getAdDetailCompanyCircles().remove(adDetailCompanyCircle);
		adDetailCompanyCircle.setCompanyCircle(null);

		return adDetailCompanyCircle;
	}

}