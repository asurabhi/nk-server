package com.nku.core.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.nku.core.domain.model.State;


@Repository
public class AdminRepositoryImpl  implements AdminRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<State> getAllStates() {
		Query allStatesQuery = em.createQuery("select distinct s from State s left join fetch s.cities c left join fetch c.localities ");
		List<State> resultList = allStatesQuery.getResultList();
		return resultList;
	}

}
