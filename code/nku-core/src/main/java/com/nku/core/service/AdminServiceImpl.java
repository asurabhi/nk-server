package com.nku.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nku.core.domain.model.City;
import com.nku.core.domain.model.State;
import com.nku.core.repository.AdminRepository;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminRepository adminRepository;

	public List<State> getAllStates() {
		return adminRepository.getAllStates();
	}

	public List<City> getCities(State state) {
		return null;
	}

}
