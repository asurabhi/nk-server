package com.nku.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.User;
import com.nku.core.repository.UserRepository;

@Service
@Transactional(readOnly=false)
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public User findUserById(Integer id) {
		return userRepository.findOne(id);
	}

}
