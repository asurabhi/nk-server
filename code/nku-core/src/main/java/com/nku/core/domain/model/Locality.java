package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the locality database table.
 * 
 */
@Entity
@NamedQuery(name="Locality.findAll", query="SELECT l FROM Locality l")
public class Locality implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="locality_id")
	private Integer localityId;

	private String landmark;

	@Column(name="loc_lang")
	private Integer locLang;

	@Column(name="loc_lat")
	private Integer locLat;

	private String name;

	private int pincode;

	//bi-directional many-to-one association to AdDetail
	@OneToMany(mappedBy="locality")
	private List<AdDetail> adDetails;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="city_id")
	private City city;

	public Locality() {
	}

	public Integer getLocalityId() {
		return this.localityId;
	}

	public void setLocalityId(Integer localityId) {
		this.localityId = localityId;
	}

	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public Integer getLocLang() {
		return locLang;
	}

	public void setLocLang(Integer locLang) {
		this.locLang = locLang;
	}

	public Integer getLocLat() {
		return locLat;
	}

	public void setLocLat(Integer locLat) {
		this.locLat = locLat;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPincode() {
		return this.pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public List<AdDetail> getAdDetails() {
		return this.adDetails;
	}

	public void setAdDetails(List<AdDetail> adDetails) {
		this.adDetails = adDetails;
	}

	public AdDetail addAdDetail(AdDetail adDetail) {
		getAdDetails().add(adDetail);
		adDetail.setLocality(this);

		return adDetail;
	}

	public AdDetail removeAdDetail(AdDetail adDetail) {
		getAdDetails().remove(adDetail);
		adDetail.setLocality(null);

		return adDetail;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

}