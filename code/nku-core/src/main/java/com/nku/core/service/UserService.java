package com.nku.core.service;

import com.nku.core.domain.model.User;

public interface UserService {

	public User findUserById(Integer id);

}
