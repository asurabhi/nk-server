package com.nku.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.City;
import com.nku.core.repository.CityRepository;

@Service
@Transactional(readOnly=false)
public class CityServiceImpl implements CityService{

	private CityRepository cityRepository;
	@Autowired
	public CityServiceImpl(CityRepository cityRepository) {
		this.cityRepository = cityRepository;	
	}
	@Override
	public City findCityById(Integer id) {
		return cityRepository.findOne(id);
	}
	@Override
	public List<City> findAllCities() {
		return cityRepository.findAll();
	}

}
