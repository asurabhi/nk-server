package com.nku.core.service;

import com.nku.core.domain.model.Category;

public interface CategoryService {

	public Category findCategoryById(Integer id);
}
 