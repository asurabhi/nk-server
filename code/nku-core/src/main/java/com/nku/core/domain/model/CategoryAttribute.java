package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the category_attributes database table.
 * 
 */
@Entity
@Table(name="category_attributes")
@NamedQuery(name="CategoryAttribute.findAll", query="SELECT c FROM CategoryAttribute c")
public class CategoryAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="category_attribute_id")
	private Integer categoryAttributeId;

	@Column(name="component_type")
	private String componentType;

	@Column(name="data_type")
	private String dataType;

	@Column(name="default_value")
	private String defaultValue;

	@Column(name="max_value")
	private int maxValue;

	@Column(name="min_value")
	private int minValue;

	private String name;

	//bi-directional many-to-one association to CategoryAttributeInstance
	@OneToMany(mappedBy="categoryAttribute")
	private List<CategoryAttributeInstance> categoryAttributeInstances;

	//bi-directional many-to-one association to CategoryAttributeOption
	@OneToMany(mappedBy="categoryAttribute")
	private List<CategoryAttributeOption> categoryAttributeOptions;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="category_id")
	private Category category;

	public CategoryAttribute() {
	}

	public Integer getCategoryAttributeId() {
		return this.categoryAttributeId;
	}

	public void setCategoryAttributeId(Integer categoryAttributeId) {
		this.categoryAttributeId = categoryAttributeId;
	}

	public String getComponentType() {
		return this.componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public int getMaxValue() {
		return this.maxValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	public int getMinValue() {
		return this.minValue;
	}

	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CategoryAttributeInstance> getCategoryAttributeInstances() {
		return this.categoryAttributeInstances;
	}

	public void setCategoryAttributeInstances(List<CategoryAttributeInstance> categoryAttributeInstances) {
		this.categoryAttributeInstances = categoryAttributeInstances;
	}

	public CategoryAttributeInstance addCategoryAttributeInstance(CategoryAttributeInstance categoryAttributeInstance) {
		getCategoryAttributeInstances().add(categoryAttributeInstance);
		categoryAttributeInstance.setCategoryAttribute(this);

		return categoryAttributeInstance;
	}

	public CategoryAttributeInstance removeCategoryAttributeInstance(CategoryAttributeInstance categoryAttributeInstance) {
		getCategoryAttributeInstances().remove(categoryAttributeInstance);
		categoryAttributeInstance.setCategoryAttribute(null);

		return categoryAttributeInstance;
	}

	public List<CategoryAttributeOption> getCategoryAttributeOptions() {
		return this.categoryAttributeOptions;
	}

	public void setCategoryAttributeOptions(List<CategoryAttributeOption> categoryAttributeOptions) {
		this.categoryAttributeOptions = categoryAttributeOptions;
	}

	public CategoryAttributeOption addCategoryAttributeOption(CategoryAttributeOption categoryAttributeOption) {
		getCategoryAttributeOptions().add(categoryAttributeOption);
		categoryAttributeOption.setCategoryAttribute(this);

		return categoryAttributeOption;
	}

	public CategoryAttributeOption removeCategoryAttributeOption(CategoryAttributeOption categoryAttributeOption) {
		getCategoryAttributeOptions().remove(categoryAttributeOption);
		categoryAttributeOption.setCategoryAttribute(null);

		return categoryAttributeOption;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}