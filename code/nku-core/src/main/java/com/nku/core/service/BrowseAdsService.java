package com.nku.core.service;

import java.util.List;

import com.nku.core.domain.model.AdDetail;
import com.nku.core.domain.model.Catalog;
import com.nku.core.domain.model.Category;
import com.nku.core.domain.model.Locality;

public interface BrowseAdsService {

	public List<Catalog> findCatalogs();
	public List<Category> findCategories(int catalogId);
	public List<AdDetail> findAds(int categoryId);
	public AdDetail findAdDetail(Integer adDetailId);
	public List<Locality> findLocalitiesOfCity(Integer cityId);
}
 