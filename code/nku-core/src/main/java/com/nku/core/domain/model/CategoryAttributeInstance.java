package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the category_attribute_instance database table.
 * 
 */
@Entity
@Table(name="category_attribute_instance")
@NamedQuery(name="CategoryAttributeInstance.findAll", query="SELECT c FROM CategoryAttributeInstance c")
public class CategoryAttributeInstance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="category_attribute_instance_id")
	private Integer categoryAttributeInstanceId;

	@Column(name="component_value")
	private String componentValue;

	//bi-directional many-to-one association to CategoryAttribute
	@ManyToOne
	@JoinColumn(name="category_attribute_id")
	private CategoryAttribute categoryAttribute;

	//bi-directional many-to-one association to AdDetail
	@ManyToOne
	@JoinColumn(name="ad_detail_id")
	private AdDetail adDetail;

	//bi-directional many-to-one association to CategoryAttributeOptionInstance
	@OneToMany(mappedBy="categoryAttributeInstance")
	private List<CategoryAttributeOptionInstance> categoryAttributeOptionInstances;

	public CategoryAttributeInstance() {
	}

	public Integer getCategoryAttributeInstanceId() {
		return this.categoryAttributeInstanceId;
	}

	public void setCategoryAttributeInstanceId(Integer categoryAttributeInstanceId) {
		this.categoryAttributeInstanceId = categoryAttributeInstanceId;
	}

	public String getComponentValue() {
		return this.componentValue;
	}

	public void setComponentValue(String componentValue) {
		this.componentValue = componentValue;
	}

	public CategoryAttribute getCategoryAttribute() {
		return this.categoryAttribute;
	}

	public void setCategoryAttribute(CategoryAttribute categoryAttribute) {
		this.categoryAttribute = categoryAttribute;
	}

	public AdDetail getAdDetail() {
		return this.adDetail;
	}

	public void setAdDetail(AdDetail adDetail) {
		this.adDetail = adDetail;
	}

	public List<CategoryAttributeOptionInstance> getCategoryAttributeOptionInstances() {
		return this.categoryAttributeOptionInstances;
	}

	public void setCategoryAttributeOptionInstances(List<CategoryAttributeOptionInstance> categoryAttributeOptionInstances) {
		this.categoryAttributeOptionInstances = categoryAttributeOptionInstances;
	}

	public CategoryAttributeOptionInstance addCategoryAttributeOptionInstance(CategoryAttributeOptionInstance categoryAttributeOptionInstance) {
		getCategoryAttributeOptionInstances().add(categoryAttributeOptionInstance);
		categoryAttributeOptionInstance.setCategoryAttributeInstance(this);

		return categoryAttributeOptionInstance;
	}

	public CategoryAttributeOptionInstance removeCategoryAttributeOptionInstance(CategoryAttributeOptionInstance categoryAttributeOptionInstance) {
		getCategoryAttributeOptionInstances().remove(categoryAttributeOptionInstance);
		categoryAttributeOptionInstance.setCategoryAttributeInstance(null);

		return categoryAttributeOptionInstance;
	}

}