package com.nku.core.domain.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the category_attribute_option_instances database table.
 * 
 */
@Entity
@Table(name="category_attribute_option_instances")
@NamedQuery(name="CategoryAttributeOptionInstance.findAll", query="SELECT c FROM CategoryAttributeOptionInstance c")
public class CategoryAttributeOptionInstance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="category_attribute_option_instance_id")
	private Integer categoryAttributeOptionInstanceId;

	//bi-directional many-to-one association to CategoryAttributeInstance
	@ManyToOne
	@JoinColumn(name="category_attribute_instance_id")
	private CategoryAttributeInstance categoryAttributeInstance;

	//bi-directional many-to-one association to CategoryAttributeOption
	@ManyToOne
	@JoinColumn(name="category_attribute_option_id")
	private CategoryAttributeOption categoryAttributeOption;

	public CategoryAttributeOptionInstance() {
	}

	public Integer getCategoryAttributeOptionInstanceId() {
		return this.categoryAttributeOptionInstanceId;
	}

	public void setCategoryAttributeOptionInstanceId(Integer categoryAttributeOptionInstanceId) {
		this.categoryAttributeOptionInstanceId = categoryAttributeOptionInstanceId;
	}

	public CategoryAttributeInstance getCategoryAttributeInstance() {
		return this.categoryAttributeInstance;
	}

	public void setCategoryAttributeInstance(CategoryAttributeInstance categoryAttributeInstance) {
		this.categoryAttributeInstance = categoryAttributeInstance;
	}

	public CategoryAttributeOption getCategoryAttributeOption() {
		return this.categoryAttributeOption;
	}

	public void setCategoryAttributeOption(CategoryAttributeOption categoryAttributeOption) {
		this.categoryAttributeOption = categoryAttributeOption;
	}

}