package com.nku.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nku.core.domain.model.City;

public interface CityRepository extends JpaRepository<City, Integer> {

}
