package com.nku.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nku.core.domain.model.Locality;

public interface LocalityReposiory extends JpaRepository<Locality, Integer> {

	public List<Locality> findByCityCityId(Integer cityId);
}
