package com.nku.core.service;

import com.nku.core.domain.model.AdDetail;

public interface AdDetailService {

	AdDetail save(AdDetail adDetail);
}
