package com.nku.core.service;

import java.util.List;

import com.nku.core.domain.model.City;

public interface CityService {

	public City findCityById(Integer id);
	public List<City> findAllCities();

}
