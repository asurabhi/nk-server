package com.nku.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nku.core.domain.model.AdDetail;

public interface AdDetailReposiory extends JpaRepository<AdDetail, Integer> {

}
