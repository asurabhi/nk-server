package com.nku.core.repository;

import java.util.List;

import com.nku.core.domain.model.State;


public interface AdminRepository  {
	
	public List<State> getAllStates() ;

}
