package com.nku.core;

import javax.persistence.EntityManager;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.City;
import com.nku.core.domain.model.State;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring/context-core-test.xml","/META-INF/spring/context-core.xml",
		"/META-INF/spring/context-dbconfig.xml" })
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class BootstrapDataTest {

	@Autowired
	private EntityManager em;

	@Test
	@Ignore
	public void simpleTest() {
		// create bootstrap data
		State ka = new State();
		ka.setName("Karnataka");
		City blr = new City();
		blr.setName("bangalore/bengaluru");
		blr.setState(ka);
		em.persist(ka);
		em.persist(blr);
	}

}
