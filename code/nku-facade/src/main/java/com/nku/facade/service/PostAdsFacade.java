package com.nku.facade.service;

import com.nku.facade.domain.AdDetailDTO;

public interface PostAdsFacade {

	public AdDetailDTO postAd(AdDetailDTO adDetailDTO);
}
