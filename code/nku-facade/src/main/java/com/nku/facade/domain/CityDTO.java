package com.nku.facade.domain;

import java.io.Serializable;
import java.util.List;

import com.nku.core.domain.model.City;

public class CityDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int cityId;
	private String name;
	private List<LocalityDTO> localities;
	public List<LocalityDTO> getLocalities() {
		return localities;
	}
	public void setLocalities(List<LocalityDTO> localities) {
		this.localities = localities;
	}
	public CityDTO(City city) {
		this.cityId = city.getCityId();
		this.name = city.getName();
	}
	public CityDTO() {
	}

	public int getCityId() {
		return this.cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}