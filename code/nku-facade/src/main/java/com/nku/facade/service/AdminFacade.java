package com.nku.facade.service;

import java.util.List;

import com.nku.facade.domain.StateDTO;

public interface AdminFacade {
	
	List<StateDTO> getAllStatesCities();

}
