package com.nku.facade.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.AdDetail;
import com.nku.core.domain.model.Catalog;
import com.nku.core.domain.model.Category;
import com.nku.core.domain.model.City;
import com.nku.core.domain.model.Locality;
import com.nku.core.service.BrowseAdsService;
import com.nku.core.service.CityService;
import com.nku.facade.domain.AdDetailDTO;
import com.nku.facade.domain.CatalogDTO;
import com.nku.facade.domain.CategoryDTO;
import com.nku.facade.domain.CityDTO;
import com.nku.facade.domain.LocalityDTO;
@Service
@Transactional(readOnly=true)
public class BrowseAdsFacadeImpl implements BrowseAdsFacade {

	private final BrowseAdsService browseAdsService;
	private final CityService cityService;
	@Autowired
	public BrowseAdsFacadeImpl(BrowseAdsService browseAdsService,CityService cityService){
		this.browseAdsService=browseAdsService;
		this.cityService=cityService;
	}
	public List<CatalogDTO> findAllCatalogs() {
		List<Catalog> catalogs = browseAdsService.findCatalogs();
		List<CatalogDTO> catalogDTOs = buildCatalogDTOs(catalogs);
		return catalogDTOs;
	}
	
	public List<CategoryDTO> findCategories(int catalogId) {
		List<Category> categories = browseAdsService.findCategories(catalogId);
		List<CategoryDTO> categoryDTOs = buildCategoryDTOs(categories);
		return categoryDTOs;
	}
	
	public List<AdDetailDTO> findAds(int categoryId) {
		List<AdDetail> adDetails = browseAdsService.findAds(categoryId);
		List<AdDetailDTO> adDetailDTOs = buildAdDetailDTOs(adDetails);
		return adDetailDTOs;
	}
	
	private List<CatalogDTO> buildCatalogDTOs(List<Catalog> catalogs) {
		List<CatalogDTO> catalogDTOs = new ArrayList<CatalogDTO>();
		for(Catalog c:catalogs){
			CatalogDTO cDTO=new CatalogDTO(c.getCatalogId(),c.getName());
			catalogDTOs.add(cDTO);
		}
		return catalogDTOs;
	}
	
	private List<CategoryDTO> buildCategoryDTOs(List<Category> categories) {
		List<CategoryDTO> categoryDTOs = new ArrayList<CategoryDTO>();
		for(Category c:categories){
			CategoryDTO cDTO = new CategoryDTO(c.getCategoryId(), c.getName());
			categoryDTOs.add(cDTO);
		}
		return categoryDTOs;
	}
	
	private List<AdDetailDTO> buildAdDetailDTOs(List<AdDetail> adDetails) {
		List<AdDetailDTO> adDetailDTOs = new ArrayList<AdDetailDTO>();
		for(AdDetail a:adDetails){
			AdDetailDTO aDTO = new AdDetailDTO(a.getAdDetailId(), a.getName());
			if(a.getDescription()!=null){
				int endIndex = Math.min(a.getDescription().length(), 50);
				aDTO.setMiniDescription(a.getDescription().substring(0,endIndex));
			}
			adDetailDTOs.add(aDTO);
		}
		return adDetailDTOs;
	}
	@Override
	public List<CityDTO> findAllCities() {
		List<City> cities = cityService.findAllCities();
		List<CityDTO> cityDTOs = new ArrayList<CityDTO>();
		for(City city:cities){
			CityDTO cityDTO = new CityDTO();
			cityDTO.setCityId(city.getCityId());
			cityDTO.setName(city.getName());
			cityDTOs.add(cityDTO);
		}
		return cityDTOs;
	}
	@Override
	public List<LocalityDTO> findLocalitiesByCity(Integer cityId) {
		List<Locality> localities = browseAdsService.findLocalitiesOfCity(cityId);
		List<LocalityDTO> localityDTOs = new ArrayList<LocalityDTO>();
		for(Locality locality:localities){
			localityDTOs.add(new LocalityDTO(locality));
		}
		return localityDTOs;
	}
	@Override
	public AdDetailDTO findAdDetail(Integer adDetailId) {
		AdDetail adDetail = browseAdsService.findAdDetail(adDetailId);
		AdDetailDTO adDetailDTO = new AdDetailDTO(adDetail.getAdDetailId(), adDetail.getName());
		adDetailDTO.setDescription(adDetail.getDescription());
		adDetailDTO.setPrice(adDetail.getPrice());
		adDetailDTO.setCityName(adDetail.getCity().getName());
		adDetailDTO.setContactNumber(adDetail.getContactNumber());
		
		return adDetailDTO;
	}

}
