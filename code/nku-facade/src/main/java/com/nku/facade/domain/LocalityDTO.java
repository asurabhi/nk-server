package com.nku.facade.domain;

import java.io.Serializable;

import com.nku.core.domain.model.Locality;
public class LocalityDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int localityId;
	private String landmark;
	private Integer locLang;
	private Integer locLat;
	private String name;
	private int pincode;

	public LocalityDTO() {
	}

	public LocalityDTO(Locality locality) {
		this.localityId = locality.getLocalityId();
		this.name = locality.getName();
		this.landmark = locality.getLandmark();
		this.locLang = locality.getLocLang();
		this.locLat = locality.getLocLat();
		this.pincode = locality.getPincode();
	}
	

	public int getLocalityId() {
		return this.localityId;
	}

	public void setLocalityId(int localityId) {
		this.localityId = localityId;
	}

	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public Integer getLocLang() {
		return locLang;
	}

	public void setLocLang(Integer locLang) {
		this.locLang = locLang;
	}

	public Integer getLocLat() {
		return locLat;
	}

	public void setLocLat(Integer locLat) {
		this.locLat = locLat;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPincode() {
		return this.pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}


}