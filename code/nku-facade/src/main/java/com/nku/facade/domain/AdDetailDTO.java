package com.nku.facade.domain;

public class AdDetailDTO {

	private Integer adDetailId;
	private String name;
	private Integer categoryId;
	private Integer userId;
	private Integer cityId;
	private String cityName;
	private Long localityId;
	private String adressline1;
	private Long contactNumber;
	private String description;
	private String miniDescription;
	private String officialEmailid;
	private String personalEmailid;
	private Boolean personalMailidVisible;
	private Boolean priceNegotiable;
	private Double price;
	public AdDetailDTO(Integer adDetailId, String name, Integer categoryId,
			Integer userId, Integer cityId, Long localityId, String adressline1,
			Long contactNumber, String description, String officialEmailid,
			String personalEmailid, Boolean personalMailidVisible,
			Boolean priceNegotiable, Double price) {
		super();
		this.adDetailId = adDetailId;
		this.name = name;
		this.categoryId = categoryId;
		this.userId = userId;
		this.cityId = cityId;
		this.localityId = localityId;
		this.adressline1 = adressline1;
		this.contactNumber = contactNumber;
		this.description = description;
		this.officialEmailid = officialEmailid;
		this.personalEmailid = personalEmailid;
		this.personalMailidVisible = personalMailidVisible;
		this.priceNegotiable = priceNegotiable;
		this.price = price;
	}
	public AdDetailDTO(Integer adDetailId, String name) {
		this.adDetailId=adDetailId;
		this.name=name;
	}
	public Integer getAdDetailId() {
		return adDetailId;
	}
	public void setAdDetailId(Integer adDetailId) {
		this.adDetailId = adDetailId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Long getLocalityId() {
		return localityId;
	}
	public void setLocalityId(Long localityId) {
		this.localityId = localityId;
	}
	public String getAdressline1() {
		return adressline1;
	}
	public void setAdressline1(String adressline1) {
		this.adressline1 = adressline1;
	}
	public Long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getMiniDescription() {
		return miniDescription;
	}
	public void setMiniDescription(String miniDescription) {
		this.miniDescription = miniDescription;
	}
	public String getOfficialEmailid() {
		return officialEmailid;
	}
	public void setOfficialEmailid(String officialEmailid) {
		this.officialEmailid = officialEmailid;
	}
	public String getPersonalEmailid() {
		return personalEmailid;
	}
	public void setPersonalEmailid(String personalEmailid) {
		this.personalEmailid = personalEmailid;
	}
	public Boolean getPersonalMailidVisible() {
		return personalMailidVisible;
	}
	public void setPersonalMailidVisible(Boolean personalMailidVisible) {
		this.personalMailidVisible = personalMailidVisible;
	}
	public Boolean getPriceNegotiable() {
		return priceNegotiable;
	}
	public void setPriceNegotiable(Boolean priceNegotiable) {
		this.priceNegotiable = priceNegotiable;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
}
