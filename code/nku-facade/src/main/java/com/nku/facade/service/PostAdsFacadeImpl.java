package com.nku.facade.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.AdDetail;
import com.nku.core.service.AdDetailService;
import com.nku.facade.domain.AdDetailDTO;

@Service
@Transactional(readOnly=false)
public class PostAdsFacadeImpl implements PostAdsFacade {

	private final AdDetailService adDetailService;
	private final Translator translator;
	@Autowired
	public PostAdsFacadeImpl(AdDetailService adDetailService,Translator translator){
		this.adDetailService= adDetailService;
		this.translator = translator;
	}
	public AdDetailDTO postAd(AdDetailDTO adDetailDTO) {
		AdDetail adDetail = translator.toAdDetail(adDetailDTO);
		AdDetail savedAdDetail = adDetailService.save(adDetail);
		return translator.toAdDetailDTO(savedAdDetail);
	}

}
