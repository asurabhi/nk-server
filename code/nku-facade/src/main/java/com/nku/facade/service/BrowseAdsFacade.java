package com.nku.facade.service;

import java.util.List;

import com.nku.facade.domain.AdDetailDTO;
import com.nku.facade.domain.CatalogDTO;
import com.nku.facade.domain.CategoryDTO;
import com.nku.facade.domain.CityDTO;
import com.nku.facade.domain.LocalityDTO;

public interface BrowseAdsFacade {

	public List<CatalogDTO> findAllCatalogs();
	public List<CategoryDTO> findCategories(int catalogId);
	public List<AdDetailDTO> findAds(int categoryId);
	public AdDetailDTO findAdDetail(Integer adDetailId);
	public List<CityDTO> findAllCities();
	public List<LocalityDTO> findLocalitiesByCity(Integer cityId);
}
