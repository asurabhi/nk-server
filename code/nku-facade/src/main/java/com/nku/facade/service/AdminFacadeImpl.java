package com.nku.facade.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nku.core.domain.model.City;
import com.nku.core.domain.model.Locality;
import com.nku.core.domain.model.State;
import com.nku.core.service.AdminService;
import com.nku.facade.domain.CityDTO;
import com.nku.facade.domain.LocalityDTO;
import com.nku.facade.domain.StateDTO;

@Service
@Transactional(readOnly=true)
public class AdminFacadeImpl implements AdminFacade {

	@Autowired
	private AdminService adminService;
	
	public List<StateDTO> getAllStatesCities() {
		List<State> allStates = adminService.getAllStates();
		List<StateDTO> statesVOList = new ArrayList<StateDTO>();
		for (State state : allStates) {
			StateDTO stateVO = new StateDTO(state);
			statesVOList.add(stateVO);
			List<CityDTO> cities = new ArrayList<CityDTO>();
			for (City city : state.getCities()) {
				CityDTO cityVO = new CityDTO(city);
				cities.add(cityVO);
				List<LocalityDTO> localitiesVO = new ArrayList<LocalityDTO>();
				for (Locality local : city.getLocalities()) {
					LocalityDTO localVO = new LocalityDTO(local);
					localitiesVO.add(localVO);
				}
				cityVO.setLocalities(localitiesVO);
			}
			stateVO.setCities(cities);
		}
		
		return statesVOList;
	}

	



}
