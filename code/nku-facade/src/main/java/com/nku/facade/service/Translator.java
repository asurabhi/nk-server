package com.nku.facade.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nku.core.domain.model.AdDetail;
import com.nku.core.repository.CategoryRepository;
import com.nku.core.repository.CityRepository;
import com.nku.core.repository.UserRepository;
import com.nku.facade.domain.AdDetailDTO;

@Component
public class Translator {
	private CategoryRepository categoryRepository;
	private CityRepository cityRepository;
	private UserRepository userRepository;
	
	@Autowired
	public Translator(CategoryRepository categoryRepository,
			CityRepository cityRepository, UserRepository userRepository) {
		super();
		this.categoryRepository = categoryRepository;
		this.cityRepository = cityRepository;
		this.userRepository = userRepository;
	}

	public AdDetail toAdDetail(AdDetailDTO adDetailDTO){
		AdDetail adDetail = new AdDetail();
		adDetail.setAdDetailId(adDetailDTO.getAdDetailId());
		adDetail.setName(adDetailDTO.getName());
		adDetail.setCategory(categoryRepository.findOne(adDetailDTO.getCategoryId()));
		adDetail.setUser(userRepository.findOne(adDetailDTO.getUserId()));
		adDetail.setCity(cityRepository.findOne(adDetailDTO.getCityId()));
		adDetail.setAdressline1(adDetailDTO.getAdressline1());
		adDetail.setContactNumber(adDetailDTO.getContactNumber());
		adDetail.setDescription(adDetailDTO.getDescription());
		adDetail.setOfficialEmailid(adDetailDTO.getOfficialEmailid());
		adDetail.setPersonalMailidVisible(adDetailDTO.getPersonalMailidVisible());
		adDetail.setPersonalMailid(adDetailDTO.getPersonalEmailid());
		adDetail.setPriceNegotiable(adDetailDTO.getPriceNegotiable());
		adDetail.setPrice(adDetailDTO.getPrice());
		return adDetail;
	}

	public AdDetailDTO toAdDetailDTO(AdDetail adDetail) {
		AdDetailDTO adDetailDTO = null;//new AdDetailDTO(adDetail.getAdDetailId(),adDetail.getName());
		return adDetailDTO;
	}
}
