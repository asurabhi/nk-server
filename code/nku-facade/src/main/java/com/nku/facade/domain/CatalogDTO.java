package com.nku.facade.domain;

public class CatalogDTO {

	private int catalogId;
	private String name;
	
	public CatalogDTO(int catalogId, String name) {
		super();
		this.catalogId = catalogId;
		this.name = name;
	}
	public int getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(int catalogId) {
		this.catalogId = catalogId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
