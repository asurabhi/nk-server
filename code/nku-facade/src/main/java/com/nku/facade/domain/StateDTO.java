package com.nku.facade.domain;

import java.io.Serializable;
import java.util.List;

import com.nku.core.domain.model.State;

public class StateDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int stateId;
	private String name;

	private List<CityDTO> cities;

	public StateDTO() {
	}

	public StateDTO(State state) {
		this.name = state.getName();
		this.stateId = state.getStateId();
	}

	public List<CityDTO> getCities() {
		return cities;
	}

	public void setCities(List<CityDTO> cities) {
		this.cities = cities;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}