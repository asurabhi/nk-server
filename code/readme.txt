* The schema can be created with the sql file located at nku-web\src\main\sqls\schema.sql file 
(preferably the db name should be nku, otherwise jdbc.properties file should be updated with the DB name you have choosen)
* The following steps can be followed to create a new module for implementing a feature.
  * Copy the nku-dummyfeature module and replace the dummy with your feature name to create a new feature module.
  * in nku-parent/pom.xml add the new module under modules section,add this new module as a dependency under dependencies section like nku-dummyfeature.
  * In the module src/main/resources/META-INF/spring/context-{feature}.xml fix the packages names.
  * In the module src/main/resources/META-INF/persistence.xml add any new entities you have created in that module.
  * run mvn dependency:analyze command in the new feature and fix the warnings it shows.
  
  
* Creating entity classes from DB tables http://shengwangi.blogspot.in/2014/12/how-to-create-java-classes-from-tables.html

* When the server started with error "java.lang.ClassNotFoundException: org.springframework.web.context.ContextLoaderListener"
follow the instructions mentioned in
http://stackoverflow.com/questions/6210757/java-lang-classnotfoundexception-org-springframework-web-context-contextloaderl



https://ngrok.com/ to expose localhost to the internet.

Google OAuth http://highaltitudedev.blogspot.in/2013/10/google-oauth2-with-jettyservlets.html
